{ sources ? import ./nix/sources.nix, pkgs ? import sources.nixpkgs { } }:

with pkgs;

poetry2nix.mkPoetryApplication { projectDir = ./.; }
