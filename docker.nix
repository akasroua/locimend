{ sources ? import ./nix/sources.nix, pkgs ? import sources.nixpkgs { } }:

with pkgs;

let locimend = callPackage ./default.nix { };

in {
  docker = dockerTools.streamLayeredImage {
    name = "locimend";
    contents = [ locimend ];
    config.Cmd = [ "api" ];
  };

}
