class Hyperparameters:
    def __init__(
        self,
        data_file,
        label_file,
        train_dataset="data/train_data.tfrecords",
        test_dataset="data/test_data.tfrecords",
        eval_dataset="data/eval_data.tfrecords",
        epochs=100,
        batch_size=64,
        learning_rate=0.004,
        l2_rate=0.001,
        max_length=80,
    ):
        self.data_file = data_file
        self.label_file = label_file
        self.train_dataset = train_dataset
        self.eval_dataset = eval_dataset
        self.test_dataset = test_dataset
        self.epochs = epochs
        self.batch_size = batch_size
        self.learning_rate = learning_rate
        self.l2_rate = l2_rate
        self.max_length = max_length
