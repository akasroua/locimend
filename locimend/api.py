from fastapi import FastAPI
from pydantic import BaseModel
from uvicorn import run

from locimend.model import infer_sequence

app = FastAPI()


class Input(BaseModel):
    sequence: str


@app.get("/{sequence}")
async def get_sequence_path(sequence: str):
    correct_sequence = await infer_sequence(sequence)
    return {"sequence": correct_sequence}


@app.post("/")
async def get_sequence_body(sequence: Input):
    correct_sequence = await infer_sequence(sequence.sequence)
    return {"sequence": correct_sequence}


def main():
    run(app, host="0.0.0.0")
